# UMB - Azure Lighthouse 
## Getting started

with this template an Azure Lighthouse connection can be established to our customers. Two deployment models are available. 
- Authorization on the complete Subscriptino
- Authorization on a specific Resource group

## Contact
Owner: Team Public Cloud (team.publiccloud.inbox@umb.ch)

## Cyber Defense Center

### CDC - Subscription Level

[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F40090140%2Frepository%2Ffiles%2Fumb-lh-security-sub.json%2Fraw)

### CDC - Resource Group Level
[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F40090140%2Frepository%2Ffiles%2Fumb-lh-security-rg.json%2Fraw)

## Managed Services

### MSP - Subscription Level

[![Deploy to Azure](https://aka.ms/deploytoazurebutton)](https://portal.azure.com/#create/Microsoft.Template/uri/https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F40090140%2Frepository%2Ffiles%2Fumb-lh-msp-sub.json%2Fraw)
